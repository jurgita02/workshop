# Workshop

[![MIT Licensed][icon-mit]][license]

## Getting started

- Create [GitLab account][3]. Strictly recommended pattern
  for the __username__: `name.lastname`, use your personal
  email (ex: `@gmail.com`)
- Create [Personal Access Token][2] with `write_repository` scope
- Request Access:
  ![requestAccess][3]
- Clone repository to your local machine

```bash
git clone https://YOUR_TOKEN_NAME:YOUR_TOKEN_VALUE@gitlab.com/iryna.v.afanasieva/workshop.git
```

If you clone without your Personal Access Token

```bash
git remote set-url origin https://YOUR_TOKEN_NAME:YOUR_TOKEN_VALUE@gitlab.com/iryna.v.afanasieva/workshop.git
```

- Create new branch, follow `feature/workshop-YOUR_USERNAME` pattern,
  lets assume that your __username__ is `john.doe`:

```bash
git checkout -b feature/workshop-john.doe
```

- Add your __username__ into [ChangeMe.md][4]:
  1. Add YOUR_USERNAME at new line in alphabetical order
  1. **Attention!** Redundant empty lines are not accepted
- Commit changes: commit message should begin with `Workshop-YOUR_USERNAME:`

```bash
git status
git add .
git status
git commit -m "Workshop-john.doe: john.doe added in alphabetical order"
```

- Push your branch into repository:

```bash
git push -u origin feature/workshop-john.doe
```

- Take a look at your `ChangeMe.md` file:
  1. All fragments from the `main` branch should not be changed
  1. Newly added `YOUR_USERNAME` in alphabetical order
- Create MR (Merge Request)
- Rebase your branch if `main` ahead:
  ![Rebase Your Branch][5]
- Resolve merge conflicts if needed
- Wait for a review

## Additional Sources

- [Git tips][6] — consolidate your knowledge of Git
- [Learn git branching][7] — improve your understanding of branching
- [Markdown Guide][8]

[icon-mit]: https://img.shields.io/badge/license-MIT-blue.svg
[license]: https://gitlab.com/gitlab-org/gitlab-foss/-/raw/master/LICENSE
[1]: https://gitlab.com/users/sign_up
[2]: https://gitlab.com/-/profile/personal_access_tokens
[3]: requestAccess.png
[4]: ChangeMe.md
[5]: rebaseYourBranch.png
[6]: http://sixrevisions.com/web-development/git-tips/
[7]: http://learngitbranching.js.org
[8]: https://www.markdownguide.org/basic-syntax/
